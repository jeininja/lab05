//Jei wen Wu - 1842614

package movies.importer;

import java.io.IOException;

public class ProcessingTest {

	public static void main (String[] args) throws IOException {
		
		String source = "C:\\Users\\jei_w\\Documents\\Programming III\\lab05\\TextInputs";
		String destination = "C:\\Users\\jei_w\\Documents\\Programming III\\lab05\\Destination";
		String destination2 = "C:\\Users\\jei_w\\Documents\\Programming III\\lab05\\Destination2";
		
		LowercaseProcessor LP = new LowercaseProcessor(source, destination, true);
		LP.execute();
		
		RemoveDuplicates RD = new RemoveDuplicates(destination, destination2, true);
		RD.execute();
	}
}
