//Jei wen Wu - 1842614

package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor{

	public RemoveDuplicates(String sourceDir, String outputDir, boolean srcContainsHeader) {
		super(sourceDir, outputDir, srcContainsHeader);
	}

	public RemoveDuplicates(String srcDir, String desDir) {
		super(srcDir, desDir, false);
	}

	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		
		ArrayList<String> removedDups = new ArrayList<String>();
		
		for(int i=0; i<input.size(); i++) {
		String s = input.get(i);
		boolean dup = removedDups.contains(s);
		if(dup == false) {
			removedDups.add(s);
		}
		}
		return removedDups;
	}
}
