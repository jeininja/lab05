//Jei wen Wu - 1842614

package movies.importer;

import java.util.ArrayList;

public class LowercaseProcessor extends Processor {

	
	public LowercaseProcessor(String sourceDir, String outputDir, boolean srcContainsHeader) {
		super(sourceDir, outputDir, srcContainsHeader);
	}
	
	
	public LowercaseProcessor(String srcDir, String desDir) {
		super(srcDir, desDir, true);
	}

	
	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		
		ArrayList<String> asLower = new ArrayList<String>();
		
		for(int i= 0; i<input.size();i++) {
			String lowerCase = input.get(i).toLowerCase();
			asLower.add(lowerCase);
		}
		return asLower;
	}
}
